# VQ/HMM based SR4X speech classification

**Note:**  All files generated as part of this exercise 
are in the `data.tgz` tarball (which expands to `./data/`).

Commands described below refer to speech wave files from the 
[SR4X v1.2 corpus sample](https://www.ohsu.edu/xd/research/centers-institutes/center-for-spoken-language-understanding/product-licensing.cfm).
With a slight file reorganization to facilitate the use of the ECOZ tools,
the files corresponding to the word `<className>` are located
(in a separate space) under `../SR4X/speech/<className>/`. 

## Endpoint detection

```
sgn.endp ../SR4X/speech/*/*[0-9].wav
```

The files from this command get also generated under the separate space
mentioned above.
They go to the same original directory and same name as
prefix, and info about the extracted interval
`<path-to-name>__S<start>_L<length>$.wav` as suffix,
where `<start>` is the start index of the detection wrt to input signal,
and `<length>` is the size of the detection.

## Predictor files

Using the endpoint-detected files above, the following starts
populating the `./data/` subdirectory here with corresponding
"predictor" files:

```
lpc -P 12 -W 45 -O 15 -m 10 -s 0.9 ../SR4X/speech/*/*$.wav
```

- `-P 12`: prediction order;
- `-W 45`: 45-ms analysis window size;
- `-O 15`: 15-ms window offset;
- `-m 10`: only consider classes with at least 10 signal files;
- `-s 0.9`: to split the set of files into approximately 90% for a
  training subset and 10% for a testing subset.
  With this option the resulting predictor files get generated under
  `data/predictors/TRAIN/` and `data/predictors/TEST/` respectively.

For the specific classification exercises, see:

- vq.md
- hmm.md
